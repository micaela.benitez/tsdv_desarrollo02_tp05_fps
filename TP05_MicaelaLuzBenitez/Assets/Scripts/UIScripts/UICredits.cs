﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LoaderManagerScript;

namespace UICreditsScript
{
    public class UICredits : MonoBehaviour
    {
        public void LoadMainMenuScene()
        {
            LoaderManager.Get().LoadScene("MainMenu");
        }
    }
}