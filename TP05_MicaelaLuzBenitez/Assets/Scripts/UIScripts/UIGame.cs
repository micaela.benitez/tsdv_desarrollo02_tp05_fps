﻿using System.Collections;
using System.Collections.Generic;
using LoaderManagerScript;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameManagerScript;
using WeaponScript;

namespace UIGameScript
{
    public class UIGame : MonoBehaviour
    {
        public TMP_Text totalPoints;
        public TMP_Text bullets;

        public Weapon weapon;

        private float minLife = 0;
        private float maxLife = 0;
        public Image lifeBar;

        private void Start()
        {
            maxLife = GameManager.Get().life;

            weapon = FindObjectOfType<Weapon>();
        }

        private void Update()
        {
            bullets.text = GameManager.Get().weapon[weapon.weaponActivated].totalBullets + "/" + GameManager.Get().weapon[weapon.weaponActivated].activeBullets;
            totalPoints.text = "Points: " + GameManager.Get().points;

            GameManager.Get().life = Mathf.Clamp(GameManager.Get().life, minLife, maxLife);
            lifeBar.fillAmount = GameManager.Get().life / maxLife;

            if (Input.GetKeyDown(KeyCode.Escape))
                LoaderManager.Get().LoadScene("MainMenu");
        }
    }
}