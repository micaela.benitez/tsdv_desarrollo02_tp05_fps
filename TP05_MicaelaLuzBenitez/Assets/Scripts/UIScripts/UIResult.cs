﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using GameManagerScript;
using LoaderManagerScript;

namespace UIResultScript
{
    public class UIResult : MonoBehaviour
    {
        public TMP_Text totalPoints;
        public TMP_Text highscore;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        private void Update()
        {
            totalPoints.text = "Points: " + GameManager.Get().points;
            highscore.text = "Points: " + GameManager.Get().highscore;
        }

        public void LoadMainMenuScene()
        {
            LoaderManager.Get().LoadScene("MainMenu");
        }
    }
}
