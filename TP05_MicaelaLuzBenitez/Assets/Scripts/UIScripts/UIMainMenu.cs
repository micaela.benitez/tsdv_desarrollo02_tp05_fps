﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LoaderManagerScript;

namespace UIMainMenuScript
{
    public class UIMainMenu : MonoBehaviour
    {
        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        public void LoadGameScene()
        {
            LoaderManager.Get().LoadScene("Game");
        }

        public void LoadCreditsScene()
        {
            LoaderManager.Get().LoadScene("Credits");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}