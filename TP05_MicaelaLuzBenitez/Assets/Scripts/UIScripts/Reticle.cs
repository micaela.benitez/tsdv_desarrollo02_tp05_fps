﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace ReticleScript
{
    public class Reticle : MonoBehaviour
    {
        private RectTransform reticle;

        public float minSize = 50;
        public float maxSize = 100;
        public float speed = 5;
        private float currentSize;

        private void Start()
        {
            reticle = GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (PlayerIsMoving())   // es lo mismo que hacer if (rb.velocity.sqrMagnitud != 0), rb -> public Rigidbody rb;
                currentSize = Mathf.Lerp(currentSize, maxSize, Time.deltaTime * speed);
            else
                currentSize = Mathf.Lerp(currentSize, minSize, Time.deltaTime * speed);

            reticle.sizeDelta = Vector2.one * currentSize;
        }

        private bool PlayerIsMoving()
        {
            if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0 || Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                return true;
            else
                return false;
        }
    }
}