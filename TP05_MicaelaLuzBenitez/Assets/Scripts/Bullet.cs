﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BulletScript
{ 
    public class Bullet : MonoBehaviour
    {
        private Rigidbody rig;
        private bool collide = false;
        private float time;

        public float speed;
        public float timerActivated;

        private void Start()
        {
            rig = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            time += Time.deltaTime;

            if (time > timerActivated)
                Destroy(gameObject);

            if (!collide)
                rig.velocity = transform.TransformDirection(new Vector3(0, 0, speed));   // Le agrego velocidad a la pelota en la direccion que disparo
        }

        private void OnCollisionEnter(Collision collision)
        {
            collide = true;
        }
    }
}