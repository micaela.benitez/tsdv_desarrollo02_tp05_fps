﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using GameManagerScript;

namespace BombScript
{    
    public class Bomb : MonoBehaviour
    {
        public delegate void BombDestroyAction(Bomb bomb);
        public BombDestroyAction OnBombDestroy;

        private FirstPersonController character;
        private float characterDistance;

        public float distanceCharacterBomb = 10;
        private bool bombTimerActivated = false;
        private float time = 0;

        [Serializable] 
        public class BombData
        {
            public float size;
            public float timer;
        }
        
        private Vector3 size;
        private float bombTimer;
        
        public void InitBomb(BombData bombData)
        {
            size = Vector3.one * bombData.size;
            bombTimer = bombData.timer;
        }
        
        private void Start()
        {
            character = FindObjectOfType<FirstPersonController>();
        }

        private void Update()
        {
            transform.localScale = size;

            characterDistance = Mathf.Sqrt(Mathf.Pow(character.transform.position.x - transform.position.x, 2) + Mathf.Pow(character.transform.position.z - transform.position.z, 2));   // Calculo la distancia bomba-personaje

            if (characterDistance < distanceCharacterBomb)   // Si la distancia es menor a distanceCharacterBomb se activa el timer de la bomba
                bombTimerActivated = true;

            if (bombTimerActivated)
            {
                time += Time.deltaTime;

                if (time > bombTimer)   // Si la bomba llega al tiempo predeterminado explota
                {
                    Destroy(gameObject);

                    if (characterDistance < distanceCharacterBomb)   // Si el personja esta cerca de la bomba pierde vida
                        GameManager.Get().LifePointsLost(GameManager.Get().bombPointsLife);
                }                    
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (OnBombDestroy != null)
                OnBombDestroy(this);
        }
    }
}