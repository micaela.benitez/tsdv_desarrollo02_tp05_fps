﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;

namespace BoxScript
{
    public class Box : MonoBehaviour
    {
        public delegate void BoxDestroyAction(Box box);
        public BoxDestroyAction OnBoxDestroy;

        [Serializable]
        public class BoxData
        {
            public float size;
        }

        private Vector3 size;

        public void InitBox(BoxData boxData)
        {
            size = Vector3.one * boxData.size;
        }

        private void Update()
        {
            transform.localScale = size;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (OnBoxDestroy != null)
                OnBoxDestroy(this);
        }
    }
}