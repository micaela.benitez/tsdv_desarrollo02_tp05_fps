﻿using BombScript;
using BoxScript;
using GameManagerScript;
using GhostAIFSMScript;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectsCreatorScript
{
    public class ObjectsCreator : MonoBehaviour
    {
        [Header("Terrain data")]
        public Terrain terrain;
        private float posX;
        private float posY;
        private float posZ;

        [Header("Bomb data")]
        public Bomb bombPrefab;
        public List<Bomb.BombData> bomb;
        public float timePerBomb = 10;
        private float bombTimer = 0;

        [Header("Box data")]
        public Box boxPrefab;
        public List<Box.BoxData> box;
        public float timePerBox = 20;
        private float boxTimer = 0;

        [Header("Ghosts data")]
        public GhostAIFSM ghostPrefab;
        public List<GhostAIFSM.GhostData> ghost;
        public float timePerGhost = 20;
        private float ghostTimer = 0;

        private void Update()
        {
            bombTimer += Time.deltaTime;
            boxTimer += Time.deltaTime;
            ghostTimer += Time.deltaTime;

            if (bombTimer > timePerBomb)
            {
                for (int i = 0; i < bomb.Count; i++)
                {
                    Bomb.BombData bombData = bomb[i];

                    CalculateTerrainPosition();
                    GameObject bombGenerator = Instantiate(bombPrefab, new Vector3(posX, posY + (bomb[i].size / 2), posZ), Quaternion.identity).gameObject;
                    bombGenerator.transform.parent = gameObject.transform;

                    Bomb b = bombGenerator.GetComponent<Bomb>();
                    b.OnBombDestroy = DestroyBomb;
                    b.InitBomb(bombData);
                }
                bombTimer = 0;
            }

            if (boxTimer > timePerBox)
            {
                for (int i = 0; i < box.Count; i++)
                {
                    Box.BoxData bombData = box[i];

                    CalculateTerrainPosition();
                    GameObject bombGenerator = Instantiate(boxPrefab, new Vector3(posX, posY + (box[i].size / 2), posZ), Quaternion.identity).gameObject;
                    bombGenerator.transform.parent = gameObject.transform;

                    Box b = bombGenerator.GetComponent<Box>();
                    b.OnBoxDestroy = DestroyBox;
                    b.InitBox(bombData);
                }
                boxTimer = 0;
            }

            if (ghostTimer > timePerGhost)
            {
                for (int i = 0; i < ghost.Count; i++)
                {
                    GhostAIFSM.GhostData ghostData = ghost[i];

                    CalculateTerrainPosition();
                    GameObject ghostGenerator = Instantiate(ghostPrefab, new Vector3(posX, posY + (ghost[i].size / 2), posZ), Quaternion.identity).gameObject;
                    ghostGenerator.transform.parent = gameObject.transform;

                    GhostAIFSM g = ghostGenerator.GetComponent<GhostAIFSM>();
                    g.OnGhostDestroy = DestroyGhost;
                    g.InitGhost(ghostData);
                }
                ghostTimer = 0;
            }
        }

        public void CalculateTerrainPosition()
        {
            posX = Random.Range(0, GameManager.Get().terrainWidth);
            posZ = Random.Range(0, GameManager.Get().terrainHeight);
            posY = terrain.terrainData.GetHeight((int)posX, (int)posZ);
        }

        public void DestroyBox(Box box)
        {
            Destroy(box.gameObject);
            GameManager.Get().PointsEarned(GameManager.Get().boxPoints);
        }

        public void DestroyBomb(Bomb bomb)
        {
            Destroy(bomb.gameObject);
            GameManager.Get().PointsEarned(GameManager.Get().bombPoints);
        }

        public void DestroyGhost(GhostAIFSM ghost)
        {
            Destroy(ghost.gameObject);
            GameManager.Get().PointsEarned(GameManager.Get().ghostPoints);
        }
    }
}

