﻿using System;
using System.Collections.Generic;
using UnityEngine;
using LoaderManagerScript;

namespace GameManagerScript
{ 
    public class GameManager : MonoBehaviour
    {
        [Serializable]
        public struct weaponData
        {
            public int bulletsPerClip;
            public int totalBullets;
            public int activeBullets;
            public float shotMaxDistance;

            [NonSerialized] public int initialBulletsPerClip;
            [NonSerialized] public int initialTotalBullets;
        }
        public weaponData[] weapon;

        public int terrainHeight = 1000;
        public int terrainWidth = 1000;
        [NonSerialized] public float highscore = 0;

        [Header("Initialization data")]
        public float life = 100;
        public int points = 0;

        private float initialLife;
        private int initialPoints;

        [Header("Objects data")]
        [Header("Bomb")]
        public int bombPointsLife = 50;
        public int bombPoints = 100;
        [Header("Box")]
        public int boxPoints = 50;
        [Header("Ghost")]
        public int ghostPointsLife = 10;   // Los puntos que te sacan los fantasmas si te tocan
        public int ghostLifePerShot = 10;   // Los puntos que le sacas al fantasma si le disparas
        public int ghostPoints = 200;   // Puntos que obetenes si matas al fantasma

        private static GameManager instance;
        public static GameManager Get()
        {
            return instance;
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
            DontDestroyOnLoad(gameObject);

            initialLife = life;
            initialPoints = points;
            for (int i = 0; i < weapon.Length; i++)
            {
                weapon[i].initialBulletsPerClip = weapon[i].bulletsPerClip;
                weapon[i].initialTotalBullets = weapon[i].totalBullets;
            }

            InitGameData();
        }

        public void InitGameData()
        {
            life = initialLife;
            points = initialPoints;
            for (int i = 0; i < weapon.Length; i++)
            {
                weapon[i].bulletsPerClip = weapon[i].initialBulletsPerClip;
                weapon[i].totalBullets = weapon[i].initialTotalBullets;
                weapon[i].activeBullets = weapon[i].bulletsPerClip;
            }
        }

        public void LifePointsLost(int points)
        {
            life -= points;

            if (life <= 0)
            {
                if (this.points > highscore)
                    highscore = this.points;
                LoaderManager.Get().LoadScene("Result");
            }
        }

        public void PointsEarned(int points)
        {
            this.points += points;
        }

        public void Recharge(int weaponActivated)
        {
            if (weapon[weaponActivated].totalBullets >= weapon[weaponActivated].bulletsPerClip)
            {
                weapon[weaponActivated].totalBullets -= weapon[weaponActivated].bulletsPerClip;
                weapon[weaponActivated].activeBullets += weapon[weaponActivated].bulletsPerClip;
            }

            if (weapon[weaponActivated].activeBullets > weapon[weaponActivated].bulletsPerClip) weapon[weaponActivated].activeBullets = weapon[weaponActivated].bulletsPerClip;
        }

        public void Shot(int weaponActivated)
        {
            if (weapon[weaponActivated].activeBullets > 0)
                weapon[weaponActivated].activeBullets--;
        }
    }
}