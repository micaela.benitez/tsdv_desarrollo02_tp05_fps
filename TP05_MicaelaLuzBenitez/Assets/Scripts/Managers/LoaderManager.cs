﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace LoaderManagerScript
{
    public class LoaderManager : MonoBehaviour
    {
        private static LoaderManager instance;
        public static LoaderManager Get()
        {
            return instance;
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
                return;
            }
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);            
        }
    }
}