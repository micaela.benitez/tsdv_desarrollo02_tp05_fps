﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;
using BulletScript;

namespace WeaponScript
{
   public class Weapon : MonoBehaviour
    {
        [Header("Weapons data")]
        public MeshRenderer[] weapon;
        [NonSerialized] public int weaponActivated = 0;

        [Header("Bullets data")]
        public Bullet bulletPrefab;
        public Bullet ballPrefab;

        private enum WEAPON { GUN, BALLTHROWER};

        private void Start()
        {
            weapon = GetComponentsInChildren<MeshRenderer>();
            ActiveGun(true);
            ActiveBallThrower(false);
        }

        private void Update()
        {
            // Cambio de arma
            if (Input.GetKeyDown(KeyCode.Alpha1)) 
                ActiveWeapon(KeyCode.Alpha1);   
            else if (Input.GetKeyDown(KeyCode.Alpha2))
                ActiveWeapon(KeyCode.Alpha2);


            Debug.DrawRay(transform.position, transform.forward * GameManager.Get().weapon[weaponActivated].shotMaxDistance, Color.red);

            if (Input.GetKeyDown(KeyCode.R))   // Recargar
                GameManager.Get().Recharge(weaponActivated);

            if (Input.GetMouseButtonDown(0))   // Disparar
            {
                if (GameManager.Get().weapon[weaponActivated].activeBullets > 0)
                {
                    GameManager.Get().Shot(weaponActivated);

                    if (weaponActivated == (int)WEAPON.GUN)   // Si el arma activa es la pistola
                        Instantiate(bulletPrefab, transform.position, transform.rotation);
                    else                                      // Si el arma activa es el lanza pelotas
                        Instantiate(ballPrefab, transform.position, transform.rotation);
                }
            }
        }   

        private void ActiveWeapon(KeyCode key)
        {
            if (key == KeyCode.Alpha1)
            {
                ActiveGun(true);
                ActiveBallThrower(false);
                weaponActivated = (int)WEAPON.GUN;
            }
            else if (key == KeyCode.Alpha2)
            {
                ActiveGun(false);
                ActiveBallThrower(true);
                weaponActivated = (int)WEAPON.BALLTHROWER;
            }
        }

        private void ActiveGun(bool state)
        {
            weapon[0].enabled = state;
            weapon[1].enabled = state;
            weapon[2].enabled = state;
            weapon[3].enabled = state;
        }

        private void ActiveBallThrower(bool state)
        {
            weapon[4].enabled = state;
            weapon[5].enabled = state;
            weapon[6].enabled = state;
            weapon[7].enabled = state;
            weapon[8].enabled = state;
        }
    }
}